module "vpc" {
  source      = "../tf-modules/modules/networking/vpc"
  environment = var.env
  vpc_cidr    = var.cidr_block
}

module "igw" {
  source      = "../tf-modules/modules/networking/igw"
  vpc_id      = module.vpc.vpc_id
  environment = var.env

}

module "ngw" {
  source         = "../tf-modules/modules/networking/nat-gw"
  eip-id         = module.eip.eip_id
  subnet-id      = flatten(module.public-subnet.*.sub_id)[0]
  environment    = var.env
  igw_depends_on = module.igw.igw_id


}
module "private-subnet" {
  source             = "../tf-modules/modules/networking/subnet"
  vpc-id             = module.vpc.vpc_id
  environment        = var.env
  subnets_cidr       = var.private_subnets_cidr
  availability_zones = var.availability_zones
  subnet-type        = var.private-subnet
}

module "public-subnet" {
  source             = "../tf-modules/modules/networking/subnet"
  vpc-id             = module.vpc.vpc_id
  environment        = var.env
  subnets_cidr       = var.public_subnets_cidr
  availability_zones = var.availability_zones
  subnet-type        = var.public-subnet
}

module "eip" {
  source         = "../tf-modules/modules/networking/eip"
  igw_depends_on = [module.igw.igw_id]
}

module "public-rt" {
  source      = "../tf-modules/modules/networking/route-table"
  vpc-id      = module.vpc.vpc_id
  environment = var.env
  subnet-type = var.public-subnet
}

module "private-rt" {
  source      = "../tf-modules/modules/networking/route-table"
  vpc-id      = module.vpc.vpc_id
  environment = var.env
  subnet-type = var.private-subnet
}

module "public-route" {
  source      = "../tf-modules/modules/networking/routes"
  rt-id       = module.public-rt.rt_id
  gw-id       = module.igw.igw_id
  subnet-type = var.public-subnet
  environment = var.env

}

module "private-route" {
  source      = "../tf-modules/modules/networking/routes"
  rt-id       = module.private-rt.rt_id
  gw-id       = module.ngw.nat_id
  subnet-type = var.private-subnet
  environment = var.env
}

module "rt-ass-public" {
  source      = "../tf-modules/modules/networking/route-associate"
  subnet-cidr = var.public_subnets_cidr
  subnet-id   = module.public-subnet.sub_id
  rt-id       = module.public-rt.rt_id
}

module "rt-ass-private" {
  source      = "../tf-modules/modules/networking/route-associate"
  subnet-cidr = var.private_subnets_cidr
  subnet-id   = module.private-subnet.sub_id
  rt-id       = module.private-rt.rt_id
}

module "security-group-bastion" {
  source            = "../tf-modules/modules/networking/security-group"
  environment       = var.env
  vpc-id            = module.vpc.vpc_id
  resource-attached = "bastion"
}

module "sg-ingress-rule" {
  source    = "../tf-modules/modules/networking/security-group-rules"
  rule-type = "ingress"
  rules     = var.sg_ingress_rules
  sg-id     = module.security-group-bastion.sg_id
}

module "sg-bastion-egress-rule" {
  source    = "../tf-modules/modules/networking/security-group-rules"
  rule-type = "egress"
  rules     = var.sg_egress_rules
  sg-id     = module.security-group-bastion.sg_id
}

module "security-group-ec2" {
  source            = "../tf-modules/modules/networking/security-group"
  environment       = var.env
  vpc-id            = module.vpc.vpc_id
  resource-attached = "instance"
}

module "sg-sg-rule" {
  source          = "../tf-modules/modules/networking/sg_to_sg_rule"
  rule-type       = "ingress"
  from_port       = 22
  to_port         = 22
  protocol        = "TCP"
  security_groups = module.security-group-bastion.sg_id
  description     = "bation SG to Private Network"
  sg-id           = module.security-group-ec2.sg_id
}

module "sg-ec2-egress-rule" {
  source    = "../tf-modules/modules/networking/security-group-rules"
  rule-type = "egress"
  rules     = var.sg_egress_rules
  sg-id     = module.security-group-ec2.sg_id
}

module "key-pair-bastion" {
  source   = "../tf-modules/modules/compute/key-pair"
  key-path = file("../.ssh/bastion_key.pub")
}

module "key-pair-ec2" {
  source   = "../tf-modules/modules/compute/key-pair"
  key-path = file("../.ssh/instance_key.pub")
}

module "bastion-box" {
  source        = "../tf-modules/modules/compute/ec2-wud"
  instance-type = var.instance_type
  subnet-id     = module.public-subnet.sub_id[0]
  sg-id         = module.security-group-bastion.sg_id
  environment   = var.env
  associate_public_ip_address = "true"
  key-name      = module.key-pair-bastion.key_name
  resource-attached = "bastion"
}

module "ec2-box" {
  source        = "../tf-modules/modules/compute/ec2-wud"
  instance-type = var.instance_type
  subnet-id     = module.private-subnet.sub_id[0]
  sg-id         = module.security-group-ec2.sg_id
  environment   = var.env
  associate_public_ip_address = "false"
  key-name      = module.key-pair-ec2.key_name
  resource-attached = "instance"
}