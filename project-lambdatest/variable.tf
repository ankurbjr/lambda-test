variable "region" {
  description = "AWS Deployment region.."
  default     = "us-east-1"
}

variable "env" {
  default = "lambda-test"
}

variable "cidr_block" {
  default = "10.0.0.0/16"
}

variable "public_subnets_cidr" {
  type    = list(any)
  default = ["10.0.1.0/24"]
}

variable "private_subnets_cidr" {
  type    = list(any)
  default = ["10.0.2.0/24"]
}

variable "availability_zones" {
  type    = list(any)
  default = ["us-east-1a"]
}

variable "public-subnet" {
  default = "public"
}

variable "private-subnet" {
  default = "private"
}


variable "sg_ingress_rules" {
  type = list(object({
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_block  = string
    description = string
  }))

  default = [
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      cidr_block  = "<your_ip>/32"
      description = "test"
    }

  ]
}

variable "sg_egress_rules" {
  type = list(object({
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_block  = string
    description = string
  }))
  default = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_block  = "0.0.0.0/0"
      description = "test"
    }
  ]
}

variable "instance_type" {
    default = "t2.micro"
}