# Lambda Test

Question 5 - 
Create a _t3a.micro {t2.micro} _ec2 instance in private subnet AWS or Azure instances using
Terraform scripts.
VPC, Subnet, RouteTable, SecurityGroup, Access Key and EC2 machines should all be created via terraform.
Use of 1 self created module is mandatory for this assignment.


Solution: -

Pre-requiste - 
- Terraform version used v0.14.6
- Need ssh agent binary. We need ssh-add command available on the workstation running terraform code.
- One can chnage instance-type by changing below value in _variable.tf_

- [ ] Sample:- 
variable "instance_type" {
    default = "t2.micro"
}
- Need to change the IP Address (google - what is my ip) inside the _variable.tf_

- [ ] Sample: -
variable "sg_ingress_rules" {
    cidr_block  = "<your_ip>/32" #Add your IP here
}

- You need to create two key using ssh-keygen
    bastion_key.pub
    instance_key.pub
  replace the same in project-lambdatest/main.tf 

- [ ] Sample: - 
  module "key-pair-ec2" {
      key-path = file("<provide_path_to_your_instance_key>")
  }
  module "key-pair-bastion" {
      key-path = file("<provide_path_to_your_instance_key>")
  }

  Approch Used:-
   - Created 2 EC2 instances
   1. Bastion instance in public subnet in order to ec2 instance
   2. EC2 instance in private subnet


# Accessing Ec2 instance on private Subnet
Execute below commands

- [ ] ssh-add <path_to_instance_key>

- [ ] ssh -A -i <path_to_bastion_key> ubuntu@<bastion_public_ip_or_dns>

Once access try to ssh ec2 instance running on private subnet

- [ ] ssh ubuntu@<private_ip_of_ec2_instance>


  
